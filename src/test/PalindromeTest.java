package test;

import main.Palindrome;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PalindromeTest {

    private final Palindrome palindrome = new Palindrome();

    @Test
    public void palindrome_word_returns_true() {
        assertTrue(palindrome.isPalindrome("kayak"));
    }

    @Test
    public void not_palindrome_word_returns_false() {
        assertFalse(palindrome.isPalindrome("keyboard"));
    }
}
