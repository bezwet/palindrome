package main;

import java.util.Scanner;

public class Main {
    private static String NOT_PALINDROME_ANSWER = "Given word is not palindrome.";
    private static String PALINDROME_ANSWER = "Given word is palindrome.";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Palindrome palindrome = new Palindrome();

        System.out.println("Provide word to be checked:");
        String word = scanner.next();

        String answer = palindrome.isPalindrome(word) ? PALINDROME_ANSWER : NOT_PALINDROME_ANSWER;

        System.out.println(answer);
    }
}