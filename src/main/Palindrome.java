package main;

public class Palindrome {

    public boolean isPalindrome(String word) {
        int start = 0;
        int end = word.length() - 1;
        char[] wordArray = word.toCharArray();
        while (start < word.length() && end > 0) {
            if (wordArray[start] != wordArray[end]) {
                return false;
            }
            start++;
            end--;
        }
        return true;

    }
}
